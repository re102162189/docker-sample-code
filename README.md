##### Maven Build
```
$ mvn clean package
```

##### Docker Build
```
$ docker build -t springio-api:1.0.0 .
```

##### Docker Run
```
$ docker run -d --name springio-api \ 
    -p 8080:8080 \
    springio-api:1.0.0

# check container status
$ docker ps -a | grep springio-api
```
##### Docker Run
```
$ curl http://localhost:8080/api/hello

$ curl http://localhost:8080/actuator/prometheus

```

##### Docker container stop and remove 
```
# stop and remove running container
$ docker stop springio-api && docker rm springio-api

# remove built Docker image 
$ docker rmi springio-api:1.0.0
```